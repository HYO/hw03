// string_array_main.cc

#include <iostream>
#include <string>
#include "string_array.h"

using namespace std;

int main() {
  StringArray str_array;
  while (true) {
    string word;
    cin >> word;
    if (word.empty()) {
      break;
    } else if (word[0] != ':') {
      str_array.Add(word);
    } else if (word == ":concat") {
      cout << "'" << str_array.Concat() << "'" << endl;
    } else if (word == ":set") {
      size_t idx;
      cin >> idx >> word;
      str_array.Set(idx, word);
      cout << "'" << str_array.Concat() << "'" << endl;
    } else if (word == ":erase") {
      size_t idx;
      cin >> idx;
      str_array.Erase(idx);
      cout << "'" << str_array.Concat() << "'" << endl;
    } else if (word == ":find") {
      cin >> word;
      cout << str_array.Find(word) << endl;
    } else {
      break;
    }
  }
  StringArray copied_str_array(str_array);
  cout << "'" << copied_str_array.Concat() << "'" << endl;
  return 0;
}

