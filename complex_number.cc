// complex_number.cc

#include "complex_number.h"


ComplexNumber::ComplexNumber() {
  real_=0;
  imag_=0;
}

ComplexNumber::ComplexNumber(const ComplexNumber& c){
  real_=c.real_;
  imag_=c.imag_;
}

ComplexNumber::ComplexNumber(double real, double imag){
  real_=real;
  imag_=imag;
}

double ComplexNumber::real() const {
  return real_;
}

double ComplexNumber::imag() const {
  return imag_;
}

void ComplexNumber::Set(double real, double imag) {
  real_=real;
  imag_=imag;
}

void ComplexNumber::Copy(const ComplexNumber& c) {
  real_=c.real_;
  imag_=c.imag_;
}

ComplexNumber ComplexNumber::Add(const ComplexNumber& c) const {
  ComplexNumber d;
  double real=real_+c.real();
  double imag=imag_+c.imag();
  d.Set(real,imag);
  return d;
}

ComplexNumber ComplexNumber::Subtract(const ComplexNumber& c) const{
  ComplexNumber d;
  double real=real_-c.real();
  double imag=imag_-c.imag();
  d.Set(real,imag);
  return d;
}

ComplexNumber ComplexNumber::Multiply(const ComplexNumber& c) const{
  ComplexNumber d;
  double real=real_*c.real()-imag_*c.imag();
  double imag=imag_*c.real()+real_*c.imag();
  d.Set(real,imag);
  return d;
}

ComplexNumber ComplexNumber::Divide(const ComplexNumber& c) const{
  ComplexNumber d;
  double real=(real_*c.real()+imag_*c.imag())/(c.real()*c.real()+c.imag()*c.imag());
  double imag=(imag_*c.real()-real_*c.imag())/(c.real()*c.real()+c.imag()*c.imag());
  d.Set(real,imag);
  return d;
}

