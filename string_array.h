// string_array.h

#ifndef _STRING_ARRAY_H_
#define _STRING_ARRAY_H_

#include <string>

class StringArray {
 public:
  StringArray();
  StringArray(const StringArray& str_array);
  ~StringArray();

  size_t size() const;
  const std::string& Get(size_t i) const;
  int Find(const std::string& str) const;
  std::string Concat() const;

  void Add(const std::string& str);
  void Erase(size_t i);
  void Set(size_t i, const std::string& str);
  void Copy(const StringArray& str_array);
  void Resize(size_t new_size);

 private:
  std::string* strings_;
  size_t size_, alloc_;
};

#endif  // _STRING_ARRAY_H_
