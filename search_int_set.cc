// search_int_set.cc

#include "search_int_set.h"
int SearchSimpleIntSet(const SimpleIntSet& s, int val){
  int k=-1,max=s.size()-1,min=0;
  if(val==s.values()[min]) return 0;
  if(val==s.values()[max]) return max;
  if(val>s.values()[max]) return k;
  while (true) {
	int mid=(max+min)/2;
	if(val>s.values()[mid]) min=mid;
	else if(val<s.values()[mid]) max= mid;
	if (val==s.values()[mid]) {k=mid; break;}
	if (max-min==1) break;
  }
  return k;
}