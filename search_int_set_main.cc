// search_int_set_main.cc

#include <iostream>
#include <vector>
#include "search_int_set.h"
#include "int_set_util.h"

using namespace std;

int main() {
  vector<int> int_array;
  if (InputIntSet(&int_array) == false) return -1;
  SimpleIntSet int_set;
  int_set.Set(&int_array[0], int_array.size());
  PrintSimpleIntSet(int_set);
  while (true) {
    int val;
    cin >> val;
    if (val == -999) break;
    cout << SearchSimpleIntSet(int_set, val) << endl;
  }
  return 0;
}

