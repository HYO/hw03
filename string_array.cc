// string_array.cc

#include "string_array.h"
StringArray::StringArray() {
  alloc_=0;
  size_=0;
  strings_=NULL;
}

StringArray::StringArray(const StringArray& str_array) {
  Copy(str_array);
}

StringArray::~StringArray() {
  size_=0;
  alloc_=0;
}

size_t StringArray::size() const {
  return size_;
} 

const std::string& StringArray::Get(size_t i) const {
 return strings_[i];
}		

int StringArray::Find(const std::string& str) const {
  int k=-1;
  for(int i=0;i<size_;i++) {
	if(str==strings_[i]) {
		k=i;
		break;
	}
  }
  return k;
}

std::string StringArray::Concat() const {
  std::string str1;
  for(int i=0; i<size_; i++) {
	str1+=strings_[i];
        if (i!=size_-1)
	  str1+=" ";
  }
  return str1;
} 

void StringArray::Add(const std::string& str) {
  Resize(size_+1);
  size_=size_+1;
  strings_[size_-1]=str;
}

void StringArray::Erase(size_t i) {
  for(int k=i;k<size_-1;k++) 
	strings_[k-1]=strings_[k];
  size_=size_-1;
}

void StringArray::Set(size_t i, const std::string& str) {
  if(i<=size_-1)
    strings_[i]=str;
  else {
    Resize(i+1);
    size_=i+1;
    strings_[i]=str;
  }
}

void StringArray::Copy(const StringArray& str_array) {
  strings_ = new std::string[str_array.size()];
  for(int i=0; i<str_array.size();i++)   
	strings_[i]=str_array.Get(i);
  size_=str_array.size();
}
void StringArray::Resize(size_t new_size) {
  std::string* str = new std::string[new_size];
  for(int i=0; i<size();i++)   
	str[i]=strings_[i];
  delete [] strings_;
  strings_ = new std::string[new_size];
  for(int i=0; i<size();i++)   
	strings_[i]=str[i];
  alloc_=size_;
  delete [] str;
}
