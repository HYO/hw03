// simple_int_set.cc
#include "simple_int_set.h"
#include "int_set_util.h"
#include <algorithm>
using namespace std;

SimpleIntSet::SimpleIntSet () {
  values_=NULL;
  size_=0;
}

SimpleIntSet::SimpleIntSet(const SimpleIntSet& int_set) {
  for(int i=0;i<int_set.size();i++)
  values_[i]=int_set.values()[i];
  size_=int_set.size();
}

SimpleIntSet::~SimpleIntSet() {
  values_=NULL;
  size_=0;
}

SimpleIntSet SimpleIntSet::Intersect(const SimpleIntSet& int_set) const{
  SimpleIntSet intset3;
  int k=0;
  int *arr;
  arr=new int [size_];
  for(int i=0; i<size_; i++) {
     for(int j=0; j<int_set.size(); j++) {
	if( values_[i]==int_set.values()[j]) {
		arr[k]=values_[i];
		k++;
		break;
	}
     }
  }
  sort(arr,arr+k);
  intset3.Set(arr,k);
  return intset3;
}

SimpleIntSet SimpleIntSet::Union(const SimpleIntSet& int_set) const{
  SimpleIntSet intset3;
  int k=0,arr[100],o;
  for(int i=0; i<size_;i++)
       arr[i]=values_[i];
 
  for(int i=0; i<int_set.size(); i++) 
	arr[i+size_]=int_set.values()[i];
  o=size_+int_set.size();
  k=o;
  for(int i=0; i<k; i++) {
     for(int j=i+1; j<k; j++) {
	if(arr[i]==arr[j]) {
		arr[i]=4535646;
		o--;
		break;
	}
     }
  }
  sort(arr,arr+k);
  intset3.Set(arr,o);
  return intset3;
}

SimpleIntSet SimpleIntSet::Difference(const SimpleIntSet& int_set) const{
  SimpleIntSet intset3;
  int k=0,o,p;
  int *arr,arr2[100];
  arr=new int [size_];
  for(int i=0; i<size_; i++) {
     for(int j=0; j<int_set.size(); j++) {
	if( values_[i]==int_set.values()[j]) {
		arr[k]=values_[i];
		k++;
		break;
	}
     }
  }
  o=size_;
  p=o;
  for(int i=0; i<size_; i++) arr2[i]=values_[i];
  for(int i=0; i<size_; i++) {
     for(int j=0; j<k; j++) {
	if(arr2[i]==arr[j]) {
		arr2[i]=9412301;
    		o=o-1;
	}
     }
  }
  sort(arr2,arr2+p);
  intset3.Set(arr2,o);
  return intset3;
}

void SimpleIntSet::Set(const int* values, size_t size) {
  int j=0;
  values_=new int[size];
  values_[0]=values[0];
  for(int i=1; i<size; i++) {
     if (values_[j]!=values[i]) {
	values_[j+1]=values[i];
	j++;
    }
  sort(values_,values_+j+1);
  size_=j+1;
  }

}
